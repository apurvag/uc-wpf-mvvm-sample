﻿using Caliburn.Micro;
using SimpleCaliburnSample.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.ViewModels
{
    public class ShellViewModel : PropertyChangedBase
    {
        private MapViewModel _mapViewModel;
        private AddFeatureLayerViewModel _addFeatureLayerViewModel;
        private readonly IEventAggregator _eventAggregator;
        private FeatureListViewModel _featureListViewModel;

        public MapViewModel MapViewModel
        {
            get { return _mapViewModel; }
            private set
            {
                _mapViewModel = value;
                NotifyOfPropertyChange(() => MapViewModel);

            }

        }

        public AddFeatureLayerViewModel AddFeatureLayerViewModel
        {
            get { return _addFeatureLayerViewModel; }
            private set
            {
                _addFeatureLayerViewModel = value;
                NotifyOfPropertyChange(() => AddFeatureLayerViewModel);

            }

        }

        public FeatureListViewModel FeatureListViewModel
        {
            get { return _featureListViewModel; }
            private set
            {
                _featureListViewModel = value;
                NotifyOfPropertyChange(() => FeatureListViewModel);

            }

        }
 
        public ShellViewModel(MapViewModel mapViewModel, 
            AddFeatureLayerViewModel addFeatureLayerViewModel,
            FeatureListViewModel featureListViewModel,
            IEventAggregator eventAggregator)
        {
            if (mapViewModel == null)
                throw new ArgumentNullException("eventAggregator is null");
            this.MapViewModel = mapViewModel;

            if (addFeatureLayerViewModel == null)
                throw new ArgumentNullException("addFeatureLayerViewModel is null");
            this.AddFeatureLayerViewModel = addFeatureLayerViewModel;

            if (featureListViewModel == null)
                throw new ArgumentNullException("featureListViewModel is null");
            this.FeatureListViewModel = featureListViewModel;

            if (eventAggregator == null)
                throw new ArgumentNullException("eventAggregator is null");
            this._eventAggregator = eventAggregator;
        }

        public void SwitchTerrain()
        {
            _eventAggregator.Publish(new SwitchBasemapTypeEvent(BasemapType.Terrain));
        }

        public void SwitchTopographic()
        {
            _eventAggregator.Publish(new SwitchBasemapTypeEvent(BasemapType.Topo));
        }

        public void SwitchStreet()
        {
            _eventAggregator.Publish(new SwitchBasemapTypeEvent(BasemapType.Street));
        }
    }
}
