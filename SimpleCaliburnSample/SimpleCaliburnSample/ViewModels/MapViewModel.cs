﻿using Caliburn.Micro;
using ESRI.ArcGIS.Client;
using ESRI.ArcGIS.Client.Tasks;
using SimpleCaliburnSample.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCaliburnSample.ViewModels
{
    public class MapViewModel : PropertyChangedBase,IHandle<SwitchBasemapTypeEvent>, IHandle<AddFeatureLayerEvent>
    {
        private LayerCollection _layers = new LayerCollection();
        private readonly IEventAggregator _eventAggregator;
   
        #region Properties
        public LayerCollection Layers
        {
            get { return _layers; }
            set
            {
                _layers = value;

                NotifyOfPropertyChange(() => Layers);
            }
        }

        public FeatureLayer FeatureLayer{get;private set;}

        #endregion

        public MapViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null)
                throw new ArgumentNullException("eventAggregator is null");
            this._eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
            Layers.Add(new ArcGISTiledMapServiceLayer() { Url = ConfigurationManager.AppSettings["StreetLayerURL"] });
        }

        public void Handle(SwitchBasemapTypeEvent message)
        {
            Layers.Clear();

            switch (message.MapType)
            {
                case BasemapType.Terrain:
                    Layers.Add(new ArcGISTiledMapServiceLayer() { Url = ConfigurationManager.AppSettings["TerrainLayerURL"] });
                    break;
                case BasemapType.Street:
                    Layers.Add(new ArcGISTiledMapServiceLayer() { Url = ConfigurationManager.AppSettings["StreetLayerURL"] });
                    break;
                case BasemapType.Topo:
                    Layers.Add(new ArcGISTiledMapServiceLayer() { Url = ConfigurationManager.AppSettings["TopoLayerURL"] });
                    break;

            }
        }

        public void Handle(AddFeatureLayerEvent message)
        {
            try
            {
                FeatureLayer = new FeatureLayer()
                {
                    Url=message.url
                   
                };

                FeatureLayer.OutFields.Add("*");

                Layers.Add(FeatureLayer);
                FeatureLayer.UpdateCompleted += FeatureLayer_UpdateCompleted;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        private void FeatureLayer_UpdateCompleted(object sender, EventArgs e)
        {
            IEnumerable<Graphic> graphics = FeatureLayer.Graphics.Take(20);
            _eventAggregator.Publish(new FeatureGraphicsEvent(graphics));
        }

       
    }
}
