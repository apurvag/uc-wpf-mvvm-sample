﻿using Caliburn.Micro;
using SimpleCaliburnSample.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.ViewModels
{
    public class AddFeatureLayerViewModel : PropertyChangedBase
    {
        private string _url = string.Empty;
        private readonly IEventAggregator _eventAggregator;
        #region Properties
        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;

                NotifyOfPropertyChange(() => Url);
            }
        }

        #endregion

        public AddFeatureLayerViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null)
                throw new ArgumentNullException("eventAggregator is null");
            this._eventAggregator = eventAggregator;
        }

        public void AddFeatureLayer()
        {
            try
            {
                _eventAggregator.Publish(new AddFeatureLayerEvent(Url));
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
    }
}
