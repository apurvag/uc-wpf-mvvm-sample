﻿using Caliburn.Micro;
using ESRI.ArcGIS.Client;
using SimpleCaliburnSample.Events;
using SimpleCaliburnSample.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.ViewModels
{
    public class FeatureListViewModel : PropertyChangedBase,IHandle<FeatureGraphicsEvent>
    {
        //http://sampleserver6.arcgisonline.com/arcgis/rest/services/ServiceRequest/FeatureServer/0
        private ObservableCollection<ServiceRequestModel> _serviceRequests = new ObservableCollection<ServiceRequestModel>();
        private readonly IEventAggregator _eventAggregator;
        
        public ObservableCollection<ServiceRequestModel> ServiceRequests
        {
            get { return _serviceRequests; }
            set
            {
                _serviceRequests = value;

                NotifyOfPropertyChange(() => ServiceRequests);
                NotifyOfPropertyChange(() => AreServiceRequestsAvailable);
            }
        }

        public bool AreServiceRequestsAvailable
        {
            get { return ServiceRequests.Count>0; }
            set
            {

                NotifyOfPropertyChange(() => AreServiceRequestsAvailable);
            }
        }

        public FeatureListViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null)
                throw new ArgumentNullException("eventAggregator is null");
            this._eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
        }

       

        public void Handle(FeatureGraphicsEvent message)
        {
            for(int k=0;k<20;k++)
            {
                Graphic g = message.Graphics.ToList()[k];
                if(g.Attributes["name"]!=null && !string.IsNullOrEmpty(g.Attributes["name"].ToString()))
                {
                    string name=g.Attributes["name"].ToString();
                    string email=g.Attributes["email"]==null?"Not available":g.Attributes["email"].ToString();
                    string comments=g.Attributes["comments"]==null?"Not available":g.Attributes["comments"].ToString();
                    string phone=g.Attributes["phone"]==null?"Not available":g.Attributes["phone"].ToString();
                    string serviceType = g.Attributes["requesttype"] == null ? null : g.Attributes["requesttype"].ToString();
                    ServiceRequestModel s = new ServiceRequestModel(name, email, phone, comments, serviceType);
                     ServiceRequests.Add(s);
                }
           
            }
        }
    }
}
