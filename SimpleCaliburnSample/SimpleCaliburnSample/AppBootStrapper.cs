﻿using Caliburn.Micro;
using SimpleCaliburnSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SimpleCaliburnSample
{
    public class AppBootStrapper:Bootstrapper<ShellViewModel> 
    {
        private SimpleContainer _container;
        private IWindowManager _windowManager=new WindowManager();
        protected override void Configure()
        {
            _container = new SimpleContainer();
            _container.RegisterSingleton(typeof(IEventAggregator), "EventAggregator", typeof(EventAggregator));
            _container.RegisterInstance(typeof(IWindowManager), "WindowManager", _windowManager);
            _container.PerRequest<ShellViewModel, ShellViewModel>();
            _container.PerRequest<MapViewModel, MapViewModel>();
            _container.PerRequest<AddFeatureLayerViewModel, AddFeatureLayerViewModel>();
            _container.PerRequest<FeatureListViewModel, FeatureListViewModel>();
        }



        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void OnUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            base.OnUnhandledException(sender, e);
        }
    }
}
