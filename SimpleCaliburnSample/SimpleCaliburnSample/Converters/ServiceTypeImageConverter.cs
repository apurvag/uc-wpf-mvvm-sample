﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SimpleCaliburnSample.Converters
{
    public class ServiceTypeImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                BitmapImage b = new BitmapImage(new Uri("/Images/snow.png", UriKind.Relative));
                return b;
            }
           

            string val = value.ToString();
            if (val=="Snow Complaint")
                return new BitmapImage(new Uri("/Images/snow.png", UriKind.Relative));
            else if(val=="Gas Station Closed")
                return new BitmapImage(new Uri("/Images/gas.png", UriKind.Relative));
            else if (val == "Insect Problem")
                return new BitmapImage(new Uri("/Images/insect.png", UriKind.Relative));
            else if (val == "Blight")
                return new BitmapImage(new Uri("/Images/tree.png", UriKind.Relative));
            else if (val == "Animal Complaint")
                return new BitmapImage(new Uri("/Images/animal.png", UriKind.Relative));

            else
                return new BitmapImage(new Uri("/Images/none.png", UriKind.Relative));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
