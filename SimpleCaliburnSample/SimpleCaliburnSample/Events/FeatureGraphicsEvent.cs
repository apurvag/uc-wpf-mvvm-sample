﻿using ESRI.ArcGIS.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.Events
{
    public class FeatureGraphicsEvent
    {
        public IEnumerable<Graphic> Graphics { get; private set; }
        public FeatureGraphicsEvent(IEnumerable<Graphic> graphics)
        {
            Graphics = graphics;
        }
    }
}
