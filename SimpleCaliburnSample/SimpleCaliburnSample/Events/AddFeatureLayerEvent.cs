﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.Events
{
    public class AddFeatureLayerEvent
    {
        public string url { get; private set; }
        public AddFeatureLayerEvent(string url)
        {
            this.url = url;
        }
    }
}
