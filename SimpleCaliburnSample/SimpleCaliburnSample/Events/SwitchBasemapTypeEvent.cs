﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.Events
{
    public class SwitchBasemapTypeEvent
    {
        public BasemapType MapType {get;private set;}

        public SwitchBasemapTypeEvent(BasemapType mapType=BasemapType.none)
        {
            MapType = mapType;
        }
    }
}
