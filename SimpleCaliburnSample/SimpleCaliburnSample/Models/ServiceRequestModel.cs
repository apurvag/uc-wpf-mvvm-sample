﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SimpleCaliburnSample.Models
{
    public class ServiceRequestModel : PropertyChangedBase
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string Comments { get; private set; }
        public string ServiceType { get; private set; }

        public ServiceRequestModel(string name,
            string email,
            string phone,
                string comments,
            string serviceType=null)
        {
            this.Name = name;
            this.Email = email;
            this.Phone = phone;
            this.Comments = comments;
            this.ServiceType = serviceType;
        }

       
    }
}
